provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::922032444791:role/TanakaTerraformExecutionRole"
  }
}

module "encryption_key" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/encryption_key?ref=v0.2.1"

  kms_keys = [
    { alias_name = "alias/tanaka-common-encryption-key" }
  ]

  tags = {
    Owner              = "tanaka"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
}
