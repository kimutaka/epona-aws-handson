# 事前課題

Eponaを利用するためには、Terraformへの理解が必須となります。  
そこで、みなさまにはTerraformによるリソース作成～削除までを一通り体験していただいた上で、ハンズオンへご参加いただきます。  
（Terraformの詳細まで理解を深める必要はありません）

## 前提条件

1. インターネット接続した状態で行って下さい
1. 作業想定時間は30分です
1. 当日は、Terraformの解説よりEponaに関する説明を優先しますので、ご注意ください

## チュートリアル

ハンズオンで利用するAWS環境は、事前準備の段階では利用不可となっています。
そこで、ローカルで実行可能なDockerを用いて体験いただきます。

下記URLを参考に「Quick start tutorial」を実行してみてください。
なお、このtutorialにはDockerも必要となります。ローカルにDockerをインストールしていない方は、下記URLの手順の通りインストールをお願いします。

- [quick-start-tutorial](https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/aws-get-started#quick-start-tutorial)

当該のtutorialをより深く理解するためには、各引数のリファレンスを参照してもOKです。  
※ハンズオンではDockerは利用しません。

- [Docker Provider](https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs)

お疲れさまでした！  
事前課題は以上となります。
