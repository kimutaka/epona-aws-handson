# バックエンド実行基盤の構築

バックエンド実行基盤を整えるためには `public_traffic_container_service` pattern[^1]を使っていきます。

[^1]: GitLabのロゴは[GitLab](https://about.gitlab.com/)によって制作されたものであり、[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)の下に提供されています。

![public_traffic_container_service](../resources/public_traffic_container_service.jpg)

## アジェンダ

`ci_pipeline` は講師の指示のもと、コードを修正していただきました。  
`public_traffic_container_service` ではドキュメントの指示に従い、受講者の皆さん自身で既存コードを修正し適用していきます。

1. [public_traffic_container_service patternについて](#public_traffic_container_service-patternについて)
   1. [依存しているパターン](#依存しているパターン)
   1. [適用後の構成図](#適用後の構成図)
1. [事前準備](#事前準備)
1. [実行基盤構築](#実行基盤構築)
   1. [backend](#backend)
      1. `public_traffic_container_service pattern` 修正
      1. `public_traffic_container_service pattern` 適用
   1. [notifier](#notifier)
      1. `public_traffic_container_service pattern` 適用

## public_traffic_container_service patternについて

`public_traffic_container_service` patternは、コンテナ化されたアプリケーションの動作環境や外部公開のために必要なリソースを構築してくれるものです。

[public_traffic_container_service pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/public_traffic_container_service/)

Eponaのドキュメントを用いて、以下について解説していきます。

* 概要
* 想定する適用対象環境
* 前提事項
* ドメインの取得について（事前に取得済み）

なお、Datadogを用いることでログを集約可能です。

### 依存しているパターン

`public_traffic_container_service pattern` は `network pattern` に依存しています。  
また、ハンズオンで利用するアプリケーション(backend, notifier)の場合、以下patternとモジュールに依存します。

|依存パターン名|依存しているリソース|
|:---|:---|
|`ci_pipeline pattern`|コンテナイメージを格納したコンテナレジストリ|
|`parameter_store pattern`|アプリケーションで利用するパラメータ|
|`database pattern`|アプリケーションで利用するデータベース|
|`redis pattern`|アプリケーションのセッション管理に利用するインメモリデータベース|
|`encryption_key pattern`|各種暗号化に利用する暗号鍵|
|(`temporary_ses_smtp_user`)|メールを送信するための認証情報|

※１ `temporary_ses_smtp_user` はpatternではありません。  
※２ アプリケーションごとに依存するpatternは異なりますので、実PJではご注意ください。

[依存するpattern](https://eponas.gitlab.io/epona/guide/patterns/aws/public_traffic_container_service/#%E4%BE%9D%E5%AD%98%E3%81%99%E3%82%8Bpattern)

Runtime環境のVPCの構築については [こちら](../../README.md#vpcの構築) にて対応済みです。  
また、ミドルウェアの構築についても [こちら](../../README.md#ミドルウェアの構築) にて対応済みです。

![public_traffic_container_service pattern 依存関係](../resources/public_traffic_container_service_dependencies.jpg)

### 適用後の構成図

上記環境から `public_traffic_container_service pattern` を適用した場合の構成図です。

![public_traffic_container_service pattern 適用後構成](../resources/public_traffic_container_service_after_apply.jpg)

---

## 事前準備

### アカウント情報の用意

事前にAWSアカウント情報をお渡ししています。以下AWSアカウント情報を用意してください。

```text
[Runtime環境]
  …
  [Terraform実行ユーザー]
    ユーザー名                  ：[LastName]StagingTerraformer
    AWSアクセスキー             ：aws_access_key_id
    AWSシークレットアクセスキー ：aws_secret_access_key
    リージョン                  ：aws_region
```

### 実行ユーザーの設定

利用するユーザーの情報を設定します。  
お好みのコマンドラインツールを開いて、以下サイトを参考に設定してください。

[AWS CLI を設定する環境変数](https://docs.aws.amazon.com/ja_jp/cli/latest/userguide/cli-configure-envvars.html)

上記設定が無事成功しているかを確認していきます。  
`$ aws sts get-caller-identity --query Arn --output text` を実行してください。

以下のように出力されたら成功です。  
※ `[LastName]` は事前に指定されたユーザー名の値が出力されていること。

```shell script
$ aws sts get-caller-identity --query Arn --output text
arn:aws:iam::938285887320:user/[LastName]StagingTerraformer
```

## 実行基盤構築

コンテナ化されたアプリケーションの動作環境や外部公開のために必要なリソース（LB, 証明書等）をAWS環境に構築します。

### ゴール

* バックエンドアプリケーション（backend, notifier）のデプロイ先となるAmazon ECSをTerraformで構築する
* 証明書やLBのためにAWS Certification Manager, Amazon Route 53などをTerraformで構築する
* 上記をAWSコンソールから確認する

---

### backend

#### public_traffic_container_service pattern 修正

public_traffic_container_serviceパターンのbackend向けディレクトリに移動します。  
※[last_name] については、事前に指定されたユーザー名のディレクトリを利用すること。

```shell script
$ cd epona-aws-handson/[last_name]/runtimes/staging/public_traffic_container_service_backend
```

##### TODOコメント対応

では、 `public_traffic_container_service_backend` ディレクトリ配下のTODOコメントに対応してください。  
全てのファイルを確認していただき、漏れがないように気を付けてください。

不明点は講師にすぐ相談してください。

##### ワークスペースの初期化

Terraformのワークスペースを初期化します。

```shell script
$ terraform init
```

以下のような表示が出ればOKです。

```shell script
$ terraform init
…
Terraform has been successfully initialized!
…
```

#### public_traffic_container_service pattern 適用

ここでは、修正したソースコードを使ってリソースを作成していきます。

##### terraform apply

変更を反映させるため、`terraform apply` します。

以下コマンドを入力してください。

```shell script
$ terraform apply
```

入力待ちの状態となります。変更を反映してよいか聞かれていればOKです。  
※ `yes` はまだ入力しないでください。

```shell script
$ terraform apply
…Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value:
```

反映させる内容を確認してください。

* `[last_name]` の修正が反映されていること
* `Plan: 27 to add, 0 to change, 0 to destroy` という出力があること

```shell script
$ terraform apply
…
   + “Name”   = [last_name]-chat-example-backend
…

Plan: 27 to add, 0 to change, 0 to destroy

…
```

問題なければ `yes` と入力し、Enterを押下してください。

* `Apply complete! Resources: 27 added, 0 changed, 0 destroyed.` という出力があること
* `Outputs` に `public_traffic_container_service_backend` についての出力がされていること

```shell script
Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value: yes

…
Apply complete! Resources: 27 added, 0 changed, 0 destroyed.

Outputs:
public_traffic_container_service_backend = {
  …
}
```

---

### notifier

notifierについては、時間の都合上講師により実施しています。  
必要があれば研修後に参照してください。

<!-- markdownlint-disable no-inline-html -->
<details>
  <summary>notifier 構築手順</summary>
<!-- markdownlint-enable no-inline-html -->

#### public_traffic_container_service pattern 修正

public_traffic_container_serviceパターンのnotifier向けディレクトリに移動します。  
※[last_name] については、事前に指定されたユーザー名のディレクトリを利用すること。

```shell script
$ cd epona-aws-handson/[last_name]/runtimes/staging/public_traffic_container_service_notifier
```

##### ワークスペースの初期化

Terraformのワークスペースを初期化します。

```shell script
$ terraform init
```

以下のような表示が出ればOKです。

```shell script
$ terraform init
…
Terraform has been successfully initialized!
…
```

#### public_traffic_container_service pattern 適用

ここでは、修正したソースコードを使ってリソースを作成していきます。

##### terraform plan

以下コマンドを実行してください。

```shell script
$ terraform plan
```

作成されるリソース（実行計画）を確認します。

* `[last_name]` の修正が反映されていること
* `Plan: 27 to add, 0 to change, 0 to destroy` という出力があること

```shell script
$ terraform plan
…
   + “Name”   = [last_name]-chat-example-ntfr
…

Plan: 27 to add, 0 to change, 0 to destroy

…
```

##### terraform apply

変更を反映させるため、`terraform apply` します。

以下コマンドを入力してください。

```shell script
$ terraform apply
```

入力待ちの状態となります。変更を反映してよいか聞かれていればOKです。  
※ `yes` はまだ入力しないでください。

```shell script
$ terraform apply
…Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value:
```

反映させる内容を確認してください。

* `[last_name]` の修正が反映されていること
* `Plan: 27 to add, 0 to change, 0 to destroy` という出力があること

```shell script
$ terraform apply
…
   + “Name”   = [last_name]-chat-example-ntfr
…

Plan: 27 to add, 0 to change, 0 to destroy

…
```

問題なければ `yes` と入力し、Enterを押下してください。

* `Apply complete! Resources: 27 added, 0 changed, 0 destroyed.` という出力があること
* `Outputs` に `public_traffic_container_service_notifier` についての出力がされていること

```shell script
Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.
  Enter a value: yes

…
Apply complete! Resources: 27 added, 0 changed, 0 destroyed.

Outputs:
public_traffic_container_service_notifier = {
  …
}
```

<!-- markdownlint-disable no-inline-html -->
</details>
<!-- markdownlint-enable no-inline-html -->

#### コンソール上から確認

リソースが無事作成されていることを確認してみましょう。講師の指示に従ってください。

[AWSコンソール](https://aws.amazon.com/jp/console/)

* Elastic Load Balancing（Application Load Balancer）
* AWS Fargate
* AWS Certificate Manager
* Amazon Route 53
* Amazon S3
* Amazon CloudWatch Logs
