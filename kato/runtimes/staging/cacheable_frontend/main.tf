# OriginとなるS3バケットを配置するリージョンを定義したAWS Provider
provider "aws" {
  alias = "origin_provider"
  assume_role {
    role_arn = "arn:aws:iam::922032444791:role/KatoTerraformExecutionRole"
  }
}

# CloudFront関連リソースを配置するリージョンを定義したAWS Provider
provider "aws" {
  alias  = "cache_provider"
  region = "us-east-1"
  assume_role {
    role_arn = "arn:aws:iam::922032444791:role/KatoTerraformExecutionRole"
  }
}

module "cacheable_frontend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cacheable_frontend?ref=v0.2.1"

  # Provider定義をmoduleに伝播
  providers = {
    aws.origin_provider = aws.origin_provider
    aws.cache_provider  = aws.cache_provider
  }

  s3_frontend_bucket_name = "kato-chat-example-frontend"
  zone_name               = "epona-handson.com"
  record_name             = "kato-chat-example.staging.epona-handson.com"
  ttl                     = "300"
  tags = {
    Owner              = "kato"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  cloudfront_default_root_object = "index.html"
  cloudfront_origin = {
    origin_path = "/public"
  }

  cloudfront_default_cache_behavior = {
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = null
    default_ttl            = null
    max_ttl                = null
    compress               = false
  }

  cloudfront_viewer_certificate = {
    acm_certificate_arn      = "arn:aws:acm:us-east-1:922032444791:certificate/d81b33f1-2cd5-4cac-a8a2-e9ff5f0d505d"
    minimum_protocol_version = "TLSv1.2_2019"
  }

  cloudfront_custom_error_responses = [
    {
      error_code            = 404
      error_caching_min_ttl = 300
      response_code         = 200
      response_page_path    = "/"
    }
  ]

  cloudfront_logging_config = {
    bucket_name           = "kato-epona-handson-frontend-logging"
    create_logging_bucket = true
    prefix                = "epona-handson-frontend"
    include_cookies       = false
  }

  # レスポンスヘッダーに情報を付与するラムダ関数名
  viewer_response_lambda_function_name = "kato-chat-example-frontend-add-header-function"

  # 以下は、PJ適用時にS3バケットの削除保護の要否に応じて設定してください
  cloudfront_logging_bucket_force_destroy = true
  s3_access_log_bucket_force_destroy      = true
  s3_frontend_bucket_force_destroy        = true
}
