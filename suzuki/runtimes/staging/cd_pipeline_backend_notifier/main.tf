provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::922032444791:role/SuzukiTerraformExecutionRole"
  }
}

module "cd_pipeline_backend_notifier" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend?ref=v0.2.1"

  delivery_account_id = "938285887320"
  ecr_repositories = {
    suzuki-chat-example-ntfr = {
      tag            = "latest" # リリース対象タグ
      artifact_name  = "ntfr"
      container_name = "IMAGE1_NAME"
    }
  }

  # Delivery環境のECRにCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_backend_triggerの output として出力される
  cross_account_codepipeline_access_role_arn = "arn:aws:iam::938285887320:role/Suzuki-Pipeline-NtfrAccessRole"
  source_bucket_name                         = "suzuki-pipeline-ntfr-source"

  cluster_name = data.terraform_remote_state.staging_public_traffic_container_service_notifier.outputs.public_traffic_container_service_notifier.ecs_cluster_name
  service_name = data.terraform_remote_state.staging_public_traffic_container_service_notifier.outputs.public_traffic_container_service_notifier.ecs_service_name

  pipeline_name                       = "suzuki-pipeline-ntfr"
  artifact_store_bucket_name          = "suzuki-ntfr-artfct"
  artifact_store_bucket_force_destroy = true # PJ適用時にS3バケットの削除保護の要否に応じて設定してください

  prod_listener_arns = [data.terraform_remote_state.staging_public_traffic_container_service_notifier.outputs.public_traffic_container_service_notifier.load_balancer_prod_listener_arn]
  target_group_names = data.terraform_remote_state.staging_public_traffic_container_service_notifier.outputs.public_traffic_container_service_notifier.load_balancer_target_group_names

  # ECSへのデプロイに使用するデプロイメントグループ名やCodeDeployのアプリケーション名をわかりやすいリソース名となるように変更
  codedeploy_app_name       = "suzuki-chat-ntfr"
  deployment_group_name_ecs = "suzuki-chat-ntfr-group"

  deployment_require_approval = false
}
