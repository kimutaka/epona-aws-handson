SCRIPT_DIR=$(cd $(dirname $0); pwd)

# インストールが必要なツールを確認
function check_tools() {
    # jqコマンドの存在チェック
    type jq > /dev/null 2>&1
    [ $?=0 ] && EXISTS_JQ=true || echo "jq command not installed"
    # terraformコマンドの存在チェック
    type terraform > /dev/null 2>&1
    [ $?=0 ] && EXISTS_TERRAFORM=true || echo "terraform command not installed"
    # awsコマンドの存在チェック
    type aws > /dev/null 2>&1
    [ $?=0 ] && EXISTS_AWS=true || echo "aws command not installed"
    # gitコマンドの存在チェック
    type git > /dev/null 2>&1
    [ $?=0 ] && EXISTS_GIT=true || echo "git command not installed"
    # jq, terraform, aws, git のいずれかが利用できない場合、異常終了する
    [ ${EXISTS_JQ} != true -a ${EXISTS_TERRAFORM} != true -a ${EXISTS_AWS} != true -a ${EXISTS_GIT} != true ] && exit -1 || :
}

# Delivery環境のAdministratorユーザのクレデンシャル情報を確認
function set_delivery_user_credential() {
    local user_name=$1
    export AWS_ACCESS_KEY_ID=$(cat ${SCRIPT_DIR}/config.json | jq -r .credentials.${user_name}.delivery.console.aws_access_key_id)
    export AWS_SECRET_ACCESS_KEY=$(cat ${SCRIPT_DIR}/config.json | jq -r .credentials.${user_name}.delivery.console.aws_secret_access_key)
    export AWS_DEFAULT_REGION=$(cat ${SCRIPT_DIR}/config.json | jq -r .credentials.${user_name}.delivery.console.aws_default_region)
    is_user ${user_name}
}

# Delivery環境のTerraform実行ユーザのクレデンシャル情報を確認
function set_delivery_terraformer_credential() {
    local user_name=$1
    export AWS_ACCESS_KEY_ID=$(cat ${SCRIPT_DIR}/config.json | jq -r .credentials.${user_name}.delivery.terraform.aws_access_key_id)
    export AWS_SECRET_ACCESS_KEY=$(cat ${SCRIPT_DIR}/config.json | jq -r .credentials.${user_name}.delivery.terraform.aws_secret_access_key)
    export AWS_DEFAULT_REGION=$(cat ${SCRIPT_DIR}/config.json | jq -r .credentials.${user_name}.delivery.terraform.aws_default_region)
    is_terraformer ${user_name}
}

# Staging環境のAdministratorユーザのクレデンシャル情報を確認
function set_staging_user_credential() {
    local user_name=$1
    export AWS_ACCESS_KEY_ID=$(cat ${SCRIPT_DIR}/config.json | jq -r .credentials.${user_name}.runtimes.staging.console.aws_access_key_id)
    export AWS_SECRET_ACCESS_KEY=$(cat ${SCRIPT_DIR}/config.json | jq -r .credentials.${user_name}.runtimes.staging.console.aws_secret_access_key)
    export AWS_DEFAULT_REGION=$(cat ${SCRIPT_DIR}/config.json | jq -r .credentials.${user_name}.runtimes.staging.console.aws_default_region)
    is_user ${user_name}
}

# Staging環境のTerraform実行ユーザのクレデンシャル情報を確認
function set_staging_terraformer_credential() {
    local user_name=$1
    export AWS_ACCESS_KEY_ID=$(cat ${SCRIPT_DIR}/config.json | jq -r .credentials.${user_name}.runtimes.staging.terraform.aws_access_key_id)
    export AWS_SECRET_ACCESS_KEY=$(cat ${SCRIPT_DIR}/config.json | jq -r .credentials.${user_name}.runtimes.staging.terraform.aws_secret_access_key)
    export AWS_DEFAULT_REGION=$(cat ${SCRIPT_DIR}/config.json | jq -r .credentials.${user_name}.runtimes.staging.terraform.aws_default_region)
    is_terraformer ${user_name}
}

# 環境変数に設定されたAdministratorユーザのクレデンシャル情報が正しいか確認
function is_user() {
    local user_name=$1
    local user_arn=$(aws sts get-caller-identity | jq -r .Arn)

    [[ ${user_arn} != *"user/${user_name}" ]] && echo "unmuch user: ${user_name}" && exit -1 || :
}

# 環境変数に設定されたTerraform実行ユーザのクレデンシャル情報が正しいか確認
function is_terraformer() {
    local user_name=$1
    local user_arn=$(aws sts get-caller-identity | jq -r .Arn)

    [[ ${user_arn} != *Terraformer ]] && echo "unmuch terraform user: ${user_name}" && exit -1 || :
}

function cd_pattern_instance_dir() {
    local user_name=$1
    local pattern_rel_path=$2
    local repository_root=$(git rev-parse --show-toplevel)

    cd ${repository_root}/${user_name}/${pattern_rel_path}
}

# TODO: apply、destroy は共通化する
function apply_pattern_instance() {
    local user_name=$1
    local pattern_instance_path=$2

    cd_pattern_instance_dir ${user_name} ${pattern_instance_path}
    terraform init
    terraform apply --auto-approve
}

function destroy_pattern_instance() {
    local user_name=$1
    local pattern_instance_path=$2

    cd_pattern_instance_dir ${user_name} ${pattern_instance_path}
    terraform init
    terraform destroy --auto-approve
}

# S3バケットを新規に作成する
function create_s3_bucket() {
    local bucket_name=$1
    local region=$2
    echo "create s3 bucket: start: ${bucket_name}, ${region}"

    # バケットの存在を確認して、存在しない場合新規にバケットを作成する
    # head-bucketで確認しているので、他の人が同名バケットを所有している場合もエラーが発生する
    if [[ -n $(aws s3api head-bucket --bucket ${bucket_name} 2>&1) ]]; then
        aws s3api create-bucket --bucket ${bucket_name} --create-bucket-configuration LocationConstraint=${region}
    fi

    echo "create s3 bucket: done: ${bucket_name}, ${region}"
}

# S3バケットを削除
function delete_s3_bucket() {
    local bucket_name=$1
    echo "delete s3 bucket: start: ${bucket_name}"

    # バケットの存在を確認して、存在している場合、削除処理を実施する
    if [[ -z $(aws s3api head-bucket --bucket ${bucket_name} 2>&1) ]]; then
        # S3バケットを空にする
        echo "delete s3 bucket: empty_s3_bucket: start: ${bucket_name}"
        empty_s3_bucket ${bucket_name}
        echo "delete s3 bucket: empty_s3_bucket: done: ${bucket_name}"

        # S3バケットを削除
        echo "delete s3 bucket: remove bucket: start: ${bucket_name}"
        aws s3 rb s3://${bucket_name} --force
        echo "delete s3 bucket: remove bucket: done: ${bucket_name}"
    fi

    echo "delete s3 bucket: done: ${bucket_name}"
}

# S3バケットのオブジェクトを全て空にする
function empty_s3_bucket() {
    local bucket_name=$1
    echo "empty s3 bucket: start: ${bucket_name}"

    # バケットの存在を確認して、存在している場合、オブジェクト削除処理を実施する
    if [[ -z $(aws s3api head-bucket --bucket ${bucket_name} 2>&1) ]]; then
        # S3バケットのバージョニングが有効かチェックする
        local bucket_versioning_status=$(aws s3api get-bucket-versioning --bucket ${bucket_name} --query "Status" --output text)
        echo "empty s3 bucket: bucket_versioning_status: ${bucket_versioning_status}"
        if [ ${bucket_versioning_status} = "Enabled" ]; then
            # バージョニングが有効であれば、バージョニングされたオブジェクトを削除
            echo "empty s3 bucket: __empty_s3_bucket_versioned_objects: start"
            __empty_s3_bucket_versioned_objects ${bucket_name}
            echo "empty s3 bucket: __empty_s3_bucket_versioned_objects: done"
        else
            # バージョニングが無効であれば、全てのオブジェクトを削除
            echo "empty s3 bucket: remove objects: start"
            aws s3 rm s3://${bucket_name} --recursive
            echo "empty s3 bucket: remove objects: done"
        fi
    fi

    echo "empty s3 bucket: done: ${bucket_name}"
}

# (内部関数)バージョニングが有効にされているS3バケットを空にする
function __empty_s3_bucket_versioned_objects() {
    local bucket_name=$1
    echo "__empty_s3_bucket_versioned_objects: start: ${bucket_name}"

    # list-objectで削除済みマーカ付きオブジェクトを一覧取得
    local result=$(aws s3api list-object-versions --bucket ${bucket_name} --query 'DeleteMarkers[].{Key:Key,VersionId:VersionId}' --output text)
    if [[ ${result} != "None" ]]; then
        echo "__empty_s3_bucket_versioned_objects: delete markered objects: start: ${result}"
        # 削除マーカーがついているオブジェクトの削除
        aws s3api list-object-versions --bucket ${bucket_name} --query 'DeleteMarkers[].{Key:Key,VersionId:VersionId}' --output text | \
            while read key versionid
            do
                echo "key: ${key} versionid: ${versionid}"
                aws s3api delete-object --bucket ${bucket_name} --key ${key} --version-id ${versionid}
            done
        echo "__empty_s3_bucket_versioned_objects: delete markered objects: done: ${result}"
    fi

    # list-objectでその他のバージョニングされているオブジェクトを一覧取得
    result=$(aws s3api list-object-versions --bucket ${bucket_name} --query 'Versions[].{Key:Key,VersionId:VersionId}' --output text)
    echo ${result}
    if [[ ${result} != "None" ]]; then
        echo "__empty_s3_bucket_versioned_objects: delete other objects: start: ${result}"
        # それ以外のオブジェクトの削除
        aws s3api list-object-versions --bucket ${bucket_name} --query 'Versions[].{Key:Key,VersionId:VersionId}' --output text | \
            while read key versionid
            do
                aws s3api delete-object --bucket ${bucket_name} --key ${key} --version-id ${versionid}
            done
        echo "__empty_s3_bucket_versioned_objects: delete other objects: end: ${result}"
    fi

    echo "__empty_s3_bucket_versioned_objects: done: ${bucket_name}"
}

# ユーザのアクセスキーを作成
# 環境を分離するのが面倒であったため、DeliveryTerraformerとStagingTerraformerを同時に処理
function create_user_access_key() {
    local path=$1
    local user_name=$2
    # user_nameをパスカルケースに変換
    local upper_user_name=$(echo ${user_name} | perl -pe 's/(^|_)./uc($&)/ge;s/_//g')
    echo "create_user_access_key: start: ${path}: ${user_name}"

    aws iam create-access-key --user-name ${upper_user_name}DeliveryTerraformer > ${path}/${upper_user_name}DeliveryTerraformer_credential.json
    aws iam create-access-key --user-name ${upper_user_name}StagingTerraformer > ${path}/${upper_user_name}StagingTerraformer_credential.json

    echo "create_user_access_key: done: ${path}: ${user_name}"
}

# ユーザのアクセスキーを作成
# 環境を分離するのが面倒であったため、DeliveryTerraformerとStagingTerraformerを同時に処理
function delete_user_access_key() {
    local user_name=$1
    # user_nameをパスカルケースに変換
    local upper_user_name=$(echo ${user_name} | perl -pe 's/(^|_)./uc($&)/ge;s/_//g')
    echo "delete_user_access_key: start ${user_name}"

    local access_key_id=$(cat ${SCRIPT_DIR}/config.json | jq -r .credentials.${user_name}.delivery.terraform.aws_access_key_id)
    aws iam delete-access-key --user-name ${upper_user_name}DeliveryTerraformer --access-key-id ${access_key_id}
    access_key_id=$(cat ${SCRIPT_DIR}/config.json | jq -r .credentials.${user_name}.runtimes.staging.terraform.aws_access_key_id)
    aws iam delete-access-key --user-name ${upper_user_name}StagingTerraformer  --access-key-id ${access_key_id}

    echo "delete_user_access_key: done: ${user_name}"
}
